//
//  ViewController.swift
//  WeatherApp
//
//  Created by Susin Dangol on 29/07/2022.
//

import UIKit
import CoreLocation

class ViewController: UIViewController, CityPickerDelegate {
    
    
    //CityPickerDelegate
    func didSelectCity(value: City, address: String) {
        self.currentAddress = address
        self.appServices.requestWeatherForLocation(value.CapitalLatitude, value.CapitalLongitude, handler: { (data, error) in
            self.toggleLoading(show: false)
            guard let weatherData = data, error == nil else{
                print("error")
                DispatchQueue.main.async {
                    self.showAlertAndAction("Oops !", "Could not fetch data.", 0)
                }
                return
            }
            DispatchQueue.main.async {
                self.goToWeatherVC(data: weatherData)
            }
        })
    }
    
    //Initializers
    let appServices = AppServices()
    let locationManager = CLLocationManager()
    
    var currentLocation: CLLocation?
    var currentAddress: String?
    var city = [City]()
    
    //outlets
    @IBOutlet weak var loadingLabel: UILabel!
    @IBOutlet weak var loading: UIActivityIndicatorView!
    @IBOutlet weak var splashLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.toggleLoading(show: false)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            self.setupLocaiton()
        }
    }
    
    
    //functions
    
    
    //location setup
    func setupLocaiton(){
        locationManager.delegate = self
        self.checkLocationServiceEnabled()
    }
    
    func checkLocationServiceEnabled(){
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
                case .denied:
                    self.showPermissionRequestAlert()
                    locationManager.startUpdatingLocation()
                case .authorizedAlways, .authorizedWhenInUse:
                    locationManager.requestWhenInUseAuthorization()
                    locationManager.startUpdatingLocation()
                case .notDetermined, .restricted:
                    locationManager.requestWhenInUseAuthorization()
                    locationManager.startUpdatingLocation()
            }
        }else{
            self.showPermissionRequestAlert()
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func goToWeatherVC(data: WeatherModel){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "WeatherVCViewController") as! WeatherVCViewController
        nextViewController.data = data.daily
        nextViewController.currentWeather = data.current
        nextViewController.address = self.currentAddress!
        nextViewController.modalPresentationStyle = .fullScreen
        self.present(nextViewController, animated:true, completion: nil)
    }
    
    func toggleLoading(show: Bool){
        if(show){
            DispatchQueue.main.async {
                self.loading.startAnimating()
                self.loading.isHidden = false
                self.loadingLabel.isHidden = false
            }
            
        }else{
            
            DispatchQueue.main.async {
                self.loading.stopAnimating()
                self.loading.isHidden = true
                self.loadingLabel.isHidden = true
            }
            
        }
    }
    
    
    func showPermissionRequestAlert(){
        let alertController = UIAlertController(title: "Location Permission Required", message: "Please enable location permissions in settings.", preferredStyle: .alert)

        let okAction = UIAlertAction(title: "Settings", style: .default, handler: {(cAlertAction) in
            //Redirect to Settings app
            UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: {(cAlertAction) in
            self.showCityPicker()
        })
        alertController.addAction(cancelAction)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }

    func showCityPicker(){
        self.toggleLoading(show: true)
        self.appServices.loadJson(filename: "cityList", completionHandler: { (cities) -> Void in
            let storyBoard: UIStoryboard = UIStoryboard(name:"Main", bundle: nil)
            let modalVC = storyBoard.instantiateViewController(withIdentifier: "CityPickerVC") as! CityPickerVC
            modalVC.city = cities
            modalVC.delegate = self
            modalVC.modalPresentationStyle = .fullScreen
            self.present(modalVC, animated: true, completion: nil)
        })
    }
    
    
    func showAlertAndAction(_ title: String!, _ msg: String!, _ from: Int!){ //from 0 -> selected location / 1 -> current location
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Try Again", style: .default , handler:{ (UIAlertAction)in
           
            if from == 0{
                 self.showCityPicker()
            }else{
                self.currentLocation = nil
                self.locationManager.requestWhenInUseAuthorization()
                self.locationManager.startUpdatingLocation()
            }
        }))
        
        if from == 1{
            alert.addAction(UIAlertAction(title: "Select Location", style: .default , handler:{ (UIAlertAction)in
                     self.showCityPicker()
            }))
        }
        
        self.present(alert, animated: true, completion: nil)
    }

}

extension ViewController:CLLocationManagerDelegate{
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if !locations.isEmpty, currentLocation == nil {
            currentLocation = locations.first
            locationManager.stopUpdatingLocation()
            self.toggleLoading(show: true)
            
            self.appServices.getAddress(currentLocation?.coordinate.latitude, currentLocation?.coordinate.longitude, handler: { [self] (address) in
                print(address)
                self.currentAddress = address
            
                self.appServices.requestWeatherForLocation(String((currentLocation?.coordinate.latitude ?? 0.0)), String(currentLocation?.coordinate.longitude ?? 0.0), handler: { (data, error) in
                    
                    self.toggleLoading(show: false)
                    
                    guard let weatherData = data, error == nil else{
                        print("error")
                        
                        DispatchQueue.main.async {
                            self.showAlertAndAction("Oops !", "Could not fetch data.", 1)
                        }
                        
                        return
                    }
                    DispatchQueue.main.async {
                        self.goToWeatherVC(data: weatherData)
                    }
                })
            })
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch CLLocationManager.authorizationStatus() {
            case .denied:
                self.showCityPicker()
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.requestWhenInUseAuthorization()
                locationManager.startUpdatingLocation()
            case .notDetermined, .restricted:
            break;
        }
    }
}

