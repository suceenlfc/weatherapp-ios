//
//  AppServices.swift
//  WeatherApp
//
//  Created by Susin Dangol on 01/08/2022.
//

import Foundation
import CoreLocation
import UIKit

class AppServices{
    
    
    typealias CompletionHandler = (_ cities:[City]) -> Void
    
    func loadJson(filename fileName: String,completionHandler: CompletionHandler) {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([City].self, from: data)
                completionHandler(jsonData)
            } catch {
                completionHandler([])
            }
        }
    }
    
    
    func requestWeatherForLocation(_ lat: String!, _ lng: String!, handler: @escaping (WeatherModel?, Error?) -> Void){
        let long = lng
        let lat = lat
        let key = "7b9829fd3398f567462a6cdaf35e6e3a"
        
        let url = "https://api.openweathermap.org/data/2.5/onecall?lat=\(lat!)&lon=\(long!)&units=imperial&exclude=hourly&appid=\(key)"
        
        print("url is", url)
        
        URLSession.shared.dataTask(with: URL(string: url)!, completionHandler: {data, response, error in
            do {
                // make sure this JSON is in the format we expect
                if let json = try JSONSerialization.jsonObject(with: data!, options: []) as? [String: Any] {
                    // try to read out a string array
                    if let cod = json["cod"] as? Int {
                        if cod == 401{
                            handler(nil, error)
                        }
                    }else{
                        //validation
                        guard let data = data, error == nil else{
                            print("something went wrong")
                            handler(nil, error)
                            return
                            
                            
                        }
                        
                        var json: WeatherModel?
                        do{
                            json = try JSONDecoder().decode(WeatherModel.self, from : data)
                        }catch{
                            handler(nil, error)
                        }
                        
                        guard let result = json else {
                            handler(nil, error)
                            return
                        }
                        
                        handler(result, nil)
                    }
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
                handler(nil, error)
            }

        }).resume()
    }
    
    func getAddress(_ lat: Double!, _ lng: Double!, handler: @escaping (String) -> Void)
    {
        var address: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude:lat!, longitude:lng)
        
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark?
            placeMark = placemarks?[0]
            
            // City
            if let city = placeMark?.addressDictionary?["City"] as? String {
                address = city
            }
            
            // locality
            if let locality = placeMark?.locality as? String {
                address = locality
            }
            
           // Passing address back
           handler(address)
        })
    }
    
    func showAlertButtonTapped(_ title: String!, _ msg: String!, _ view: UIViewController) {
        // create the alert
        let alert = UIAlertController(title: title, message: msg, preferredStyle: UIAlertController.Style.alert)

        // add the actions (buttons)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: nil))

        // show the alert
        view.present(alert, animated: true, completion: nil)
    }
}
