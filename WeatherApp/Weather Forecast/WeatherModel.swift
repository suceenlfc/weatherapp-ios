//
//  WeatherModel.swift
//  WeatherApp
//
//  Created by Susin Dangol on 31/07/2022.
//

import Foundation


struct WeatherModel: Codable {
    let lat, lon: Double?
    let timezone: String?
    let timezone_offset: Int?
    let current: Current?
    let daily: [Daily?]
}

// MARK: - Current
struct Current: Codable {
    let dt: Int?
    let temp, feels_like, dew_point, uvi: Double?
    let pressure, humidity: Int?
    let clouds, visibility: Int?
    let wind_speed: Double?
    let wind_deg: Int?
    let wind_gust: Double?
    let weather: [Weather?]
}

// MARK: - Weather
struct Weather: Codable {
    let id: Int?
    let main, description, icon: String?
}

// MARK: - Daily
struct Daily: Codable {
    let dt, sunrise, sunset, moonrise: Int?
    let moonset: Int?
    let moon_phase: Double?
    let temp: Temp?
    let feels_like: FeelsLike?
    let pressure, humidity: Int?
    let dew_point, wind_speed: Double?
    let wind_deg: Int?
    let wind_gust: Double?
    let weather: [Weather?]
    let clouds: Int?
    let pop: Double?
    let uvi: Double?
}

// MARK: - FeelsLike
struct FeelsLike: Codable {
    let day, night, eve, morn: Double?
}

// MARK: - Temp
struct Temp: Codable {
    let day, min, max, night: Double?
    let eve, morn: Double?
}
