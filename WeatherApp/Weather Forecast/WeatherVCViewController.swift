//
//  WeatherVCViewController.swift
//  WeatherApp
//
//  Created by Susin Dangol on 31/07/2022.
//


import UIKit

class WeatherVCViewController: UIViewController, CityPickerDelegate {
    func didSelectCity(value: City, address: String) {
        
        self.appServices.requestWeatherForLocation(value.CapitalLatitude, value.CapitalLongitude, handler: { (data, error) in
            
            guard let weatherData = data, error == nil else{
                print("error")
                self.appServices.showAlertButtonTapped("Oops !", "Could not fetch data. Try again later", self)
                return
            }
            
            self.address = address
            self.currentWeather = weatherData.current
            self.data = weatherData.daily
            self.data.removeFirst()
            DispatchQueue.main.async {
                
                self.tableView.tableHeaderView = self.createTableHeader()
                self.animateTableHeaderView()
                self.tableView.reloadData()
            }
        })
    }
    
    
    let appServices = AppServices()
    
    var data = [Daily?]()
    var currentWeather: Current?
    var address = String()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        data.removeFirst()
        
        self.view.backgroundColor = UIColor(red: 67/255.0, green: 139/255.0, blue: 181/255.0, alpha: 1.0)
        tableView.backgroundColor = UIColor(red: 67/255.0, green: 139/255.0, blue: 181/255.0, alpha: 1.0)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableHeaderView = self.createTableHeader()
        
    }
    
    @objc func mapImageTapped(_ sender: UITapGestureRecognizer) {
        self.appServices.loadJson(filename: "cityList", completionHandler: { (cities) -> Void in
            let storyBoard: UIStoryboard = UIStoryboard(name:"Main", bundle: nil)
            let modalVC = storyBoard.instantiateViewController(withIdentifier: "CityPickerVC") as! CityPickerVC
            modalVC.city = cities
            modalVC.delegate = self
            modalVC.modalPresentationStyle = .fullScreen
            self.present(modalVC, animated: true, completion: nil)
        })
    }
    
    
    func animateTableHeaderView(){
        

        self.tableView.tableHeaderView!.transform = CGAffineTransform(scaleX: 0, y: 0)
        self.tableView.tableHeaderView!.backgroundColor = UIColor(red: 67/255.0, green: 139/255.0, blue: 181/255.0, alpha: 1.0)
        
        UIView.animate(withDuration: 0.5, delay: 0.01, options: .transitionCurlUp ,animations: {
            self.tableView.tableHeaderView!.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.tableView.tableHeaderView!.backgroundColor = UIColor(red: 49/255.0, green: 96/255.0, blue: 130/255.0, alpha: 1.0)
        })
    }
    
    func createTableHeader() -> UIView {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.width))
        headerView.backgroundColor = UIColor(red: 49/255.0, green: 96/255.0, blue: 130/255.0, alpha: 1.0)
        
        let locationLabel = UILabel()
        let mapImage = UIImageView()
        let detailsView = UIView()
        let currentlyLabel = UILabel()
        let tempLabel = UILabel()
        let weatherSummaryLabel = UILabel()
        let weatherImage = UIImageView()
        
        let mapImageTap = UITapGestureRecognizer(target: self, action: #selector(self.mapImageTapped(_:)))
        mapImage.isUserInteractionEnabled = true
        mapImage.addGestureRecognizer(mapImageTap)
        
        

        locationLabel.translatesAutoresizingMaskIntoConstraints = false
        mapImage.translatesAutoresizingMaskIntoConstraints = false
        detailsView.translatesAutoresizingMaskIntoConstraints = false
        currentlyLabel.translatesAutoresizingMaskIntoConstraints = false
        weatherImage.translatesAutoresizingMaskIntoConstraints = false
        weatherSummaryLabel.translatesAutoresizingMaskIntoConstraints = false
        tempLabel.translatesAutoresizingMaskIntoConstraints = false
        
        headerView.addSubview(locationLabel)
        headerView.addSubview(mapImage)
        headerView.addSubview(detailsView)
        detailsView.addSubview(currentlyLabel)
        detailsView.addSubview(tempLabel)
        detailsView.addSubview(weatherSummaryLabel)
        headerView.addSubview(weatherImage)
       
        NSLayoutConstraint.activate([
            locationLabel.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 20),
            locationLabel.centerXAnchor.constraint(equalTo: headerView.centerXAnchor),
            locationLabel.heightAnchor.constraint(equalToConstant: headerView.frame.size.height/5),
            locationLabel.widthAnchor.constraint(equalToConstant: view.frame.size.width-20)
        ])
        
        NSLayoutConstraint.activate([
            mapImage.topAnchor.constraint(equalTo: headerView.topAnchor, constant: 20),
            mapImage.trailingAnchor.constraint(equalTo: headerView.trailingAnchor, constant: -15),
            mapImage.heightAnchor.constraint(equalToConstant: 30),
            mapImage.widthAnchor.constraint(equalToConstant: 30)
        ])
        
        NSLayoutConstraint.activate([
            detailsView.topAnchor.constraint(equalTo: locationLabel.bottomAnchor, constant: 30),
            detailsView.leadingAnchor.constraint(equalTo: headerView.leadingAnchor, constant: 5),
            detailsView.heightAnchor.constraint(equalToConstant: headerView.frame.size.height - 20),
            detailsView.widthAnchor.constraint(equalToConstant: headerView.frame.size.width*0.5)
        ])
        
        NSLayoutConstraint.activate([
            currentlyLabel.topAnchor.constraint(equalTo: detailsView.topAnchor),
            currentlyLabel.leadingAnchor.constraint(equalTo: detailsView.leadingAnchor),
            currentlyLabel.trailingAnchor.constraint(equalTo: detailsView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            tempLabel.topAnchor.constraint(equalTo: currentlyLabel.bottomAnchor, constant: 10),
            tempLabel.leadingAnchor.constraint(equalTo: detailsView.leadingAnchor),
            tempLabel.trailingAnchor.constraint(equalTo: detailsView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            weatherSummaryLabel.topAnchor.constraint(equalTo: tempLabel.bottomAnchor, constant: 10),
            weatherSummaryLabel.leadingAnchor.constraint(equalTo: detailsView.leadingAnchor),
            weatherSummaryLabel.trailingAnchor.constraint(equalTo: detailsView.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            weatherImage.topAnchor.constraint(equalTo: locationLabel.bottomAnchor, constant: 30),
            weatherImage.leadingAnchor.constraint(equalTo: detailsView.trailingAnchor, constant: 10),
            weatherImage.heightAnchor.constraint(equalToConstant: 150),
            weatherImage.widthAnchor.constraint(equalToConstant: 150)
        ])
        
    
        currentlyLabel.text = "CURRENTLY"
        currentlyLabel.textAlignment = .center
        locationLabel.textAlignment = .center
        tempLabel.textAlignment = .center
        weatherSummaryLabel.textAlignment = .center
        
        mapImage.image = UIImage(named: "map")
        
        locationLabel.text = self.address
        locationLabel.font = tempLabel.font.withSize(30)
        
        tempLabel.text = "\(self.convertToCelsius(fahrenheit: (self.currentWeather?.temp)!))°C"
        tempLabel.font = tempLabel.font.withSize(40)
        
        weatherImage.image = UIImage(named: self.setWeatherImage(type: self.currentWeather?.weather[0]?.main ?? ""))
        
        weatherSummaryLabel.text = self.currentWeather?.weather[0]?.description
        
        return headerView
    }
    
    func getDay(timestamp: Int) -> String{
        let unixTimestamp = timestamp
        let date = Date(timeIntervalSince1970: Double(unixTimestamp))
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: date)
    }
    
    func convertToCelsius(fahrenheit: Double) -> String {
//        return String(String(5.0 / 9.0 * (fahrenheit - 32.0)).prefix(2))
        
        var temp_string = String(5.0 / 9.0 * (fahrenheit - 32.0))
        var celsius = temp_string.components(separatedBy: ".")
        return celsius[0]
    }
    
    func setWeatherImage(type: String) -> String {
        switch type{
            case "Thunderstorm":
                return "thunderstorm"
        case "Drizzle":
            return "drizzle"
        case "Rain":
            return "rainy"
        case "Snow":
            return "snow"
        case "Clear":
            return "sunny"
        case "Clouds":
            return "cloudy"
        default:
            return "atmosphere"
        }
    }
    
    

}

extension WeatherVCViewController: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.data.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherListCell", for: indexPath) as! WeatherListCell
        
        cell.isUserInteractionEnabled = false
        
        cell.dayLabel.text = self.getDay(timestamp:self.data[indexPath.row]?.dt ?? 0)
        cell.weatherImg.image = UIImage(named: self.setWeatherImage(type: self.data[indexPath.row]?.weather[0]?.main ?? ""))
        cell.lowTemp.text = "\(self.convertToCelsius(fahrenheit: self.data[indexPath.row]?.temp?.min ?? 0.0))°C"
        cell.highTemp.text = "\(self.convertToCelsius(fahrenheit: self.data[indexPath.row]?.temp?.max ?? 0.0))°C"
        
        cell.backgroundColor = UIColor(red: 67/255.0, green: 139/255.0, blue: 181/255.0, alpha: 1.0)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        cell.transform = CGAffineTransform(scaleX: 0, y: 0)
        
        UIView.animate(withDuration: 0.5, delay: 0.05 * Double(indexPath.row), options: .curveEaseInOut ,animations: {
            cell.transform = CGAffineTransform(scaleX: 1, y: 1)
        })
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
      
    }
    
    
}


