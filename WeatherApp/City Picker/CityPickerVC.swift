//
//  CityPickerVC.swift
//  WeatherApp
//
//  Created by Susin Dangol on 29/07/2022.
//

import UIKit

protocol CityPickerDelegate {
    func didSelectCity(value : City, address: String)
}

class CityPickerVC: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var cityListView: UITableView!
    
    
    var delegate : CityPickerDelegate!
    
    var city = [City]()
    var city_filter = [City]()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.city = self.loadJson(filename: "cityList") ?? []
        print("city are", self.city)
        searchBar.delegate = self
        cityListView.delegate = self
        cityListView.dataSource = self
        
    }
    
    func loadJson(filename fileName: String) -> [City]? {
        if let url = Bundle.main.url(forResource: fileName, withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode([City].self, from: data)
                return jsonData
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
    

    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return self.searchBar.text?.isEmpty ?? true
    }

}

extension CityPickerVC: UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.searchBarIsEmpty()) ? self.city.count : self.city_filter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityListCell", for: indexPath) as! CityPickerListCell
        
//e        cell.label.text = self.arrList[indexPath.row]
        cell.cityName.text = (self.searchBarIsEmpty()) ? self.city[indexPath.row].CapitalName : self.city_filter[indexPath.row].CapitalName
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        
        let value = (self.searchBarIsEmpty()) ? self.city[indexPath.row] : self.city_filter[indexPath.row]
        let address = value.CapitalName

        self.delegate.didSelectCity(value: value, address: address)
        self.dismiss(animated: true, completion: nil)
        
      
    }
    
    
}

extension CityPickerVC: UISearchBarDelegate{
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        print(searchText)
        self.city_filter = self.city.filter({( item : City) -> Bool in
            return item.CapitalName.lowercased().contains(searchText.lowercased())
        })
        cityListView.reloadData()
    }
}

struct CitiesData: Decodable {
    var city: [City]
}
struct City : Decodable, Equatable {
    var CountryName: String
    var CapitalName: String
    var CapitalLatitude: String
    var CapitalLongitude: String
}
